package input;

public class ProfileInput {

//	private int id ;//id番号
	private int age;//個数
	private String  tel;//電話番号
	private String name;//名前
	private String gender;//性別
	private String birthday;//生年月日
	private String address;//住所
	private String mail;//メールアドレス




//	public ProfileInput() {}
//
//	public int getId() {
//		return id;
//	}
//	public void setId(int id) {
//		this.id = id;
//	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @param id
	 * @param age
	 * @param tel
	 * @param name
	 * @param gender
	 * @param birthday
	 * @param　address
	 * @param mail
	 */
	public ProfileInput(/*int id,*/ int age, String tel, String name, String gender, String birthday, String address,
			String mail) {
		super();
		/*this.id = id;*/
		this.age = age;
		this.tel = tel;
		this.name = name;
		this.gender = gender;
		this.birthday = birthday;
		this.address = address;
		this.mail = mail;
	}
}
