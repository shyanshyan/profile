package sql;

import java.sql.Connection;//特定のデータベースとの接続(セッション)を表現します。
import java.sql.DriverManager;//一連のJDBCドライバを管理するための基本的なサービスです。
//注: JDBC 2.0 APIで新しく追加されたDataSourceインタフェースを使用してデータ・ソースに接続することも可能です。
import java.sql.PreparedStatement;//プリコンパイルされたSQL文を表すオブジェクトです。
import java.sql.SQLException;//データベース・アクセス・エラーまたはその他のエラーに関する情報を提供する例外です。

import input.ProfileInput;

public class ProfileSql {

	public boolean create(ProfileInput p) {
		// Connection型のconn変数にnullを代入
		Connection conn = null;

		final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
		final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
		final String DB_NAME = "Shinya";// データベース名
		final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防
		final String DB_USER = "root";// ユーザーID
		final String DB_PASS = "root";// パスワード
		try {
			// JDBCドライバの読み込み
			Class.forName(DRIVER_NAME);
			// データベース、ユーザーID、パスワード読み込みconnに代入
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

			// INSERT文を準備
			String sql = "INSERT INTO Profile( age, tel, name, gender, birthday, address, mail) "
					+ "VALUES(?,?,?,?,?,?,?)";// Profileテーブルのレコードに値を追加
			//sqlを実行する準備状態にする
			PreparedStatement pStmt = conn.prepareStatement(sql);
//			pStmt.setInt(1, p.getId());
			//pStemの中のメッソトに値を入れる
			pStmt.setInt(1, p.getAge());
			pStmt.setString(2, p.getTel());
			pStmt.setString(3, p.getName());
			pStmt.setString(4, p.getGender());
			pStmt.setString(5, p.getBirthday());
			pStmt.setString(6, p.getAddress());
			pStmt.setString(7, p.getMail());
			//resultのデータをMySQLにアップデートする
			int result  = pStmt.executeUpdate();
			if(result !=1){
				return false;
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return true;

	}
}
